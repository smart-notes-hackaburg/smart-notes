let button = document.querySelector(".button");
let speechRecognition = new webkitSpeechRecognition();

// Create a buffer to store the incoming data.
let chunks = [];
let mediaRecorder = undefined;

let recordingBool = false;
let final_transcript = "";
document.getElementById("button").innerHTML = "\u2B24";


speechRecognition.continuous = true;
speechRecognition.interimResults = true;

let promise;
let globalResolve;


speechRecognition.onresult = (event) => {
    let interim_transcript = "";
    for (let i = event.resultIndex; i < event.results.length; ++i) {
        if (event.results[i].isFinal) {
            final_transcript += event.results[i][0].transcript;
        } else {
            interim_transcript += event.results[i][0].transcript;
        }
    }
    document.querySelector("#final").innerHTML = final_transcript;
    document.querySelector("#interim").innerHTML = interim_transcript;
};

speechRecognition.onend = () => {
    //Thank you an Johannes!
    globalResolve?.();
    promise = undefined;
    globalResolve = undefined;
}

document.querySelector("button").onclick = () => {
    if (navigator.mediaDevices) {
        console.log('getUserMedia supported.');
        navigator.mediaDevices.getUserMedia({audio: true})
            .then(function (stream) {

                // Set up the AudioContext.
                //const audioCtx = new AudioContext();
                //const microphone = audioCtx.createMediaStreamSource(stream);

                if (mediaRecorder == undefined) {
                    mediaRecorder = new MediaRecorder(stream);

                    mediaRecorder.ondataavailable = (event) => {
                        chunks.push(event.data);
                    }

                    mediaRecorder.onstop = () => {
                        // A "blob" combines all the audio chunks into a single entity
                        const blob = new Blob(chunks, {"type": "audio/ogg; codecs=opus"});
                        chunks = []; // clear buffer

                        // One of many ways to use the blob
                        const audio = new Audio();
                        const audioURL = window.URL.createObjectURL(blob);
                        audio.src = audioURL;
                        console.log(audio);
                        document.getElementById('audio-player').src = audioURL;
                    }
                }

                recordingBool = !recordingBool;
                console.log(recordingBool);
                if (recordingBool) {
                    final_transcript += " Speaker: ";
                    speechRecognition.start();
                    mediaRecorder.start();
                    console.log("Recording started");
                    document.getElementById("button").innerHTML = "II";
                } else {
                    mediaRecorder.stop();
                    speechRecognition.stop();
                    promise = new Promise((resolve) => {
                        globalResolve = resolve;
                    })
                    document.getElementById("button").innerHTML = "\u2B24";
                    document.getElementById("button").innerHTML = "\u2B24";
                    if (promise) {
                        promise.then(() => final_transcript += "<br>" + "<br>");
                    } else {
                        final_transcript += "<br>" + "<br>";
                    }
                    // document.getElementById('answerBox').style.height=auto;
                }

            })
            .catch(function (err) {
                console.log('The following gUM error occurred: ' + err);
            });
    } else {
        console.log('getUserMedia not supported on your browser!');
    }
};

function saveToTxt() {
    var textToSave = final_transcript;

    var hiddenElement = document.createElement('a');

    hiddenElement.href = 'data:attachment/text,' + encodeURI(textToSave);
    hiddenElement.target = '_blank';
    hiddenElement.download = 'transcript.txt';
    hiddenElement.click();
};