from speechbrain.pretrained import SpeakerRecognition, EncoderDecoderASR
import speechbrain as sb
import torchaudio
import argparse

def speaker_recognition():
    verification = SpeakerRecognition.from_hparams(source='speechbrain/spkrec-ecapa-voxceleb', savedir='pretrained_model')

    signal1, fs = torchaudio.load('./audiofiles/kristin-hi.wav')
    signal2, fs = torchaudio.load('./audiofiles/kristin-hello.wav')

    score, prediction = verification.verify_batch(signal1, signal2)
    print(prediction.item())
    f = open("audio_comparison_result.txt", "a")
    type(prediction)
    f.write(str(prediction.item()))
    f.close()


def speech_recognition():
    model = EncoderDecoderASR.from_hparams(source="speechbrain/asr-wav2vec2-commonvoice-fr", savedir='./tmp')
    audio = './audiofiles/kristin-hi.wav'
    normalized = EncoderASR.normalizer(audio, 44500)
    audio_enc = model.encode_file(normalized)
    transcribed = model.transcribe(audio_enc)

    # transcribed = model.transcribe_file('./audiofiles/kristin-hi.wav')
    print(transcribed)


if __name__ == '__main__':
    speaker_recognition()
    # speech_recognition()
